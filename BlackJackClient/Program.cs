﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackLibrary;

namespace BlackJackClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Shoe shoe = new Shoe();
            Hand hand = shoe.NewPlayer();
            Hand dealer = shoe._dealersHand;

            //A loop for testing only
            for (int i = 0; i < 3; i++)
            {
                shoe.NewPlayer();
            }

            shoe.shuffleUpAndDeal();
            Console.WriteLine($"The dealer has: {dealer._handValue} his cards are: {dealer._hand[0].ToString()} and {dealer._hand[1].ToString()}");

            shoe.Deal();
            Console.WriteLine($"The dealer has: {dealer._handValue} his cards are: {dealer._hand[0].ToString()} and {dealer._hand[1].ToString()}");

        }
    }
}
