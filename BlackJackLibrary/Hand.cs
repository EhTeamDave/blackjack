﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackLibrary
{
    public class Hand
    {
        public List<Card> _hand = new List<Card>();
        private int _value = 0;
        
        public int _handValue { 
            get 
            {
                return _value;
            } }
        public void Add(Card card)
        {
            _hand.Add(card);
            int nonAceValue = 0;
            int numAces = 0;
            foreach (Card c in _hand)
            {
                if (c._rank == Card.Rank.Ace) // if the card is an ace
                {
                    ++numAces;
                }
                else
                    nonAceValue += c._value;
            }
            if (numAces > 0)
            { 
                if(numAces == 1)
                {
                    if (nonAceValue + 11 <= 21)
                    {
                        _value = nonAceValue + 11;
                    }
                    else
                        _value = ++nonAceValue;
                }
                else if(numAces == 2)
                {
                    if (nonAceValue + 12 <= 21)
                    {
                        _value = nonAceValue + 12;
                    }
                    else
                        _value = nonAceValue + 2;
                }
                else if(numAces == 3)
                {
                    if (nonAceValue + 13 <= 21)
                    {
                        _value = nonAceValue + 13;
                    }
                    else
                        _value = nonAceValue + 3;
                }
                else
                {
                    if (nonAceValue + 14 <= 21)
                    {
                        _value = nonAceValue + 14;
                    }
                    else
                        _value = nonAceValue + 4;
                }
            }
            else
                _value = nonAceValue;
        }
    }
}
