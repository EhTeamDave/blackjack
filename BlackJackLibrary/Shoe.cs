﻿/*
 * Program:         Shoe.cs
 * Date:            March 26, 2020
 * Author:          Dave Pettit, Fumiko Suzuki, Sam Penn
 * Description:     Provide a Shoe definition for a Blackjack game.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackLibrary
{
    public interface IShoe
    {
        void Shuffle();
        Card Draw();
    }

    public class Shoe : IShoe
    {
        #region Class members and Constructor
        private List<Card> _deck = null;
        public List<Hand> _table = null;
        private int _nextCard;
        private int _cutCard;
        public Hand _dealersHand { get { return _table[0]; } }


        public Shoe()
        {
            _deck = new List<Card>();
            _table = new List<Hand>();
            _table.Add(new Hand()); // the dealer
        }

        #endregion

        #region Class Methods

        //the table can hold unlimited players, represented by Hand objects
        public Hand NewPlayer()
        {
            Hand hand = new Hand();
            _table.Add(hand);
            return hand;
        }

        public void Shuffle()
        {
            Random rng = new Random();
            _deck = _deck.OrderBy(card => rng.Next()).ToList();
            _cutCard = rng.Next(20, 45); // set the cutcard to prevent card counting, remove a random amount of cards from the game.
            _nextCard = 0;
        }

        public Card Draw()
        {
            if (_nextCard >= (156 - _cutCard))
                throw new IndexOutOfRangeException("Time to shuffle.");
            return _deck[_nextCard++];
        }
        
        public void shuffleUpAndDeal()
        {
            // Clear out the "old" cards
            _deck.Clear();

            // Add new "new" cards
            for (int d = 0; d < 3; ++d) //3 decks in the shoe
            {
                foreach (Card.Suit s in Enum.GetValues(typeof(Card.Suit)))
                {
                    foreach (Card.Rank r in Enum.GetValues(typeof(Card.Rank)))
                    {
                        _deck.Add(new Card(s, r));
                    }
                }
            }

            // Randomize the collection
            Shuffle();
            //Deal the cards
            Deal();
           

            //This needs to be moved to game play area, when it's the dealers turn to play
            /*if (dealersHand._hand.Count == 2 && dealersHand._handValue == 17)
            {
                IEnumerable<Card> anAce = dealersHand._hand.Where(c => c._rank == Card.Rank.Ace);
                if (anAce.Count() > 0)
                {
                    dealersHand.Add(this.Draw());
                }
                else
                    Console.WriteLine("Dealer stands with 17");
            }*/
        }

        public void Deal()
        {
            //if the hand has cards clear it
            foreach(Hand h in _table)
            {
                if(h._hand.Count > 0)
                    h._hand.Clear();
            }

            //deal the cards, each player gets two cards
            for (int j = 0; j < 2; j++)
            {
                for (int i = 1; i < _table.Count; i++)
                {
                    _table[i].Add(this.Draw());
                }
                _dealersHand.Add(this.Draw());  //Deals to the dealer last on each round of dealing
            }
        }

        

        #endregion

    } 
}
