﻿/*
 * Program:         Card.cs
 * Date:            March 26, 2020
 * Author:          Dave Pettit, Fumiko Suzuki, Sam Penn
 * Description:     Provide a card definition for a Blackjack game.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackLibrary
{
    public class Card
    {
        public enum Suit { Clubs, Diamonds, Hearts, Spades };
        public enum Rank { Ace, King, Queen, Jack, Ten, Nine, Eight, Seven, Six, Five, Four, Three, Two };

        public Suit _suit { get; private set; }

        public Rank _rank { get; private set; }

        public int _value { get; }

        public override string ToString()
        {
            return _rank.ToString() + " of " + _suit.ToString();
        }

        public Card(Suit s, Rank r)
        { 
            _suit = s;
            _rank = r;
            switch (this._rank)
            {
                case Rank.Ace:
                    _value = 1;
                    break;
                case Rank.Two:
                    _value = 2;
                    break;
                case Rank.Three:
                    _value = 3;
                    break;
                case Rank.Four:
                    _value = 4;
                    break;
                case Rank.Five:
                    _value = 5;
                    break;
                case Rank.Six:
                    _value = 6;
                    break;
                case Rank.Seven:
                    _value = 7;
                    break;
                case Rank.Eight:
                    _value = 8;
                    break;
                case Rank.Nine:
                    _value = 9;
                    break;
                case Rank.Ten:
                    _value = 10;
                    break;
                case Rank.Jack:
                    _value = 10;
                    break;
                case Rank.Queen:
                    _value = 10;
                    break;
                case Rank.King:
                    _value = 10;
                    break;
            }
        }
    }
}
